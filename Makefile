USER=__update_the_cpanel_username_here__
STAGING_HOST=223.27.19.115
PRODUCTION_HOST=223.27.19.32

DRUSH_REMOTE_STAGING=ssh $(USER)@$(STAGING_HOST) drush -r public_html
DRUSH_REMOTE_PRODUCTION=ssh $(USER)@$(PRODUCTION_HOST) drush8 -r public_html

ci-setup:
	mkdir -p ~/.ssh
	cat deploy/known_hosts >> ~/.ssh/known_hosts
	(umask  077 ; echo $$SSH_KEY | base64 --decode > ~/.ssh/id_rsa)

ci-deploy-staging:
	rsync -e 'ssh' -irKzl --delete --verbose --exclude-from="deploy/.deploy-ignore" "./" $(USER)@$(STAGING_HOST):public_html

ci-deploy-production:
	rsync -e 'ssh' -irKzl --delete --verbose --exclude-from="deploy/.deploy-ignore" "./" $(USER)@$(PRODUCTION_HOST):public_html

ci-pre-deploy:
	if [[ -f sites/default/settings.php ]] ; then $($(DRUSH)) vset maintenance_mode 1; else echo "Nothing to do"; fi

ci-post-deploy:
	if [[ -f sites/default/settings.php ]] ; then $($(DRUSH)) updb -y; else echo "Nothing to do"; fi
	if [[ -f sites/default/settings.php ]] ; then $($(DRUSH)) vset maintenance_mode 0; else echo "Nothing to do"; fi
