<?php

/**
 * @file
 * om_license_agreement.tpl.php
 */
?>

<div class="om-license">
  <div class="content">
    <h2>Licence Agreement</h2>
    <h3>1 Operative part</h3>
    <p>The terms and conditions contained in this End User Licence Agreement constitute a legally binding contract between Online Medical Pty Ltd ACN 141 416 220 (Company, Licensor, We, Our, Us and other similar terms) and apply to the contractual arrangement with the End User. The contact information for the Company is:</p>
    <p>Online Medical Pty Ltd<br>
    Professor Andrew Renaut, Director<br>
    259 Wickham Terrace<br>
    Spring Hill QLD 4000<br>
    Telephone: 07 3831 9471<br>
    Nominated email address: admin@onlinemedical.com.au<br>
    </p>
    <h4>1.1 Offer</h4>
    <p>The terms and conditions in this End User Licence Agreement amount to an offer by (End User, You, Your and other similar terms) to the Company.</p>
    <h4>1.2 Acknowledgment</h4>
    <ul>
      <li>a) You acknowledge and agree that You have had sufficient opportunity to read and understand the terms and conditions by which We are prepared to accept Your Offer and that You are legally able to make this Offer.</li>
      <li>b) You acknowledge that these terms and conditions were brought to Your attention prior to making the Offer to enter into this Agreement.</li>
      <li>c) In making the Offer, You warrant that: 
        <ul>
          <li>i. the information provided regarding Your contact information is accurate; and </li>
          <li>ii. We may contact You to confirm information in relation to this offer.</li>
        </ul>
      </li>
    </ul>
    <p>
      <strong>If You are not prepared to be bound by these terms and condition of this Licence You must immediately notify the Company.</strong>
    </p>
    <h3>2. Definitions</h3>
    <h4>2.1 Defined terms</h4>
    <p>In this Agreement, unless the context or subject matter otherwise requires: Agreement means the terms and conditions contained in this End User Licence Agreement and any annexures.</p>
    <p>Commencement Date means When the Company uploads and installs the Website, notwithstanding that this Agreement was agreed to before or after this time. Conditions Precedent means those things that must be satisfied and continue in full force and effect as provided at 3.1 of this Agreement.</p>
    <p>Intellectual Property means all industrial and intellectual property rights including, without limitation, patents, copyright, right to extract information from databases, design rights, trade secrets, rights of confidence, and all forms of protection of a similar nature or having similar effect to any of them which may subsist anywhere in the world (whether or not any of them are registered and including applications and the right to make applications for registration of any of them). Licence Fee means the consideration payable to the Licensor as part of the Conditions Precedent, which are collateral to this Agreement. Moral Rights means any moral rights including the rights described in Article 6 is of the Berne Convention for Protection of Literary and Artistic Works 1886 (as amended and revised from time to time), being &quot;droit moral&quot; or other analogous rights arising under any statute (including the Copyright Act 1968 (Cth) or any other law of the Commonwealth of Australia), that exist or that may come to exist, anywhere in the World.</p>
    <p>Party means a party to this Agreement.</p>
    <p>Program means the software described as Online Medical Modules, which utilise proprietary code written in PHP, JavaScript or other languages that do not form part of the Drupal content management system.</p>
    <p>Source Code means the source of the Program, containing variable declarations, instructions, functions, loops, and other statements that tell the Program how to function.</p>
    <p>Term means the period of twenty (20) Years from the Commencement Date.</p>
    <h4>2.2 Interpretation</h4>
    <p>The rules of interpretation that apply are contained in the Terms of Business – Website Design and Development, which are available at: http://www.onlinemedical.com.au/terms-and- conditions/website-design- and-development- terms-business.</p>
    <h3>3. Commencement and Term</h3>
    <p>This Agreement commences on the Commencement Date and continues for the Term unless terminated according to its terms.</p>
    <h4>3.1 Conditions Precedent</h4>
    <p>This Agreement is collateral to and conditional on the Parties entering into a Contract for Website Design and Development, Hosting Terms of Service and Hosting Service Level Agreement on commercial terms.</p>
    <h3>4. Grant of rights</h3>
    <h4>4.1 Licence</h4>
    <p>Subject to the satisfaction of the Conditions Precedent, the Licensor grants the End User a Non-exclusive, Non-transferrable, Non-Sublicenceable and Limited Licence to use Program for the Term.</p>
    <h4>4.2 Revocation</h4>
    <ul>
      <li>a) The Licence contained in this Agreement is irrevocable, provided that the Conditions Precedent between the Parties:
        <ul>
          <li>i. are being complied with in all respects; or</li>
          <li>ii. or have been validly terminated according to these terms.</li>
        </ul>
      </li>
      <li>
        b) The End User agrees to:
        <ul>
          <li>i. use the Program only for the purpose for which it was created;</li>
          <li>ii. otherwise comply with its obligations contained in this Agreement.</li>
        </ul>
      </li>
    </ul>
    <h4>4.3 Ownership</h4>
    <ul>
      <li>a) The Licensor retains all right, title and interest in all Intellectual Property in the Program.</li>
      <li>b) Each End User acknowledges that:
        <ul>
          <li>i. it does not acquire any Intellectual Property rights, either express or implied, in the Program beyond the terms contained in this Agreement; and</li>
          <li>ii. that it does not acquire any right to access to the Source Code that may be associated with the Program.</li>
        </ul>
      </li>
    </ul>
    <h4>4.4 Licence Fee</h4>
    <p>The End User agrees that the Licence Fee forms part of the Consideration paid to the Licensor for completing the Conditions Precedent.</p>
    <h4>4.5 Specific prohibitions</h4>
    <ul>
      <li>
        a) The End User agrees that it must not:
        <ul>
          <li>i. use the Program for any purpose or in any manner other than as set out in clause 4.1;</li>
          <li>ii. infringe the Licensor’s or any third party’s Intellectual Property in the Program;</li>
          <li>iii. permit any unauthorised third party to obtain access to the Program or the Source Code;</li>
          <li>iv. reproduce, make error corrections to or otherwise modify or adapt the Intellectual Property in the Program or create any derivative works based on the Program;</li>
          <li>v. de-compile, disassemble, decrypt, or otherwise reverse engineer the Program or permit any third party to do so;</li>
          <li>vi. modify or remove any copyright or proprietary notices associated with the Program.</li>
        </ul>
      </li>
      <li>b) The End User acknowledges that the Licensor has absolute discretion in determining whether the End User has committed a prohibited act contained in clause 4.5(a).</li>
    </ul>
    <h3>5. No copies except for backup and security</h3>
    <ul>
      <li>a) The End Users must not copy the Program, except for back-up and security purposes, in this regard:
        <ul>
          <li>i. the copies belong to the Licensor and are subject to this Agreement as if it were the licensed copy of the Program;</li>
          <li>ii. the End User must not use the copies for any other purpose;</li>
          <li>iii. the End User must ensure that the copies bears a notice of the Licensor’s ownership (if possible);</li>
          <li>iv. the Licensor may decide the form and content of the notice mentioned in 5(a)(iii).</li>
        </ul>
      </li>
    </ul>
    <h3>6. Licensors warranties</h3>
    <ul>
      <li>a) The Licensor warrants that:
        <ul>
          <li>i. it owns the Intellectual Property Rights in the Program and that it’s use does not and will not infringe any rights of third parties (including, without limitation, any Intellectual Property Rights);</li>
          <li>ii. the provision of any Services does not and will not infringe the rights (including, but not limited to, Intellectual Property Rights) of any third party;</li>
          <li>iii. it will, procure all licences and consents to use any Intellectual Property Rights of a third party which are necessary to provide the Licence.</li>
        </ul>
      </li>
    </ul>
    <h3>7. Indemnity</h3>
    <ul>
      <li>a) The End User indemnifies, defends and holds harmless the Licensor in respect of all actions, claims, proceedings, demands, liabilities, losses, damages, expenses and costs (including legal fees on a full indemnity basis), in connection with:
        <ul>
          <li>i. any breach of this Agreement by the End User;</li>
          <li>ii. the End User’s negligent acts or omissions; or</li>
          <li>iii. the use of the Program, including any third party claims made in connection with, or arising out of, the End User’s use of the Program.</li>
        </ul>
      </li>
    </ul>
    <h3>8. Termination</h3>
    <h4>8.1 Termination by the Licensor</h4>
    <p>The Licensor can terminate this Agreement without notice if:</p>
    <ul>
      <li>a) it no longer has the right to provide the Program to the End User for any reason whatsoever;</li>
      <li>b) the End User commits a breach of any of an essential term of this Agreement which is not capable of being remedied; or</li>
      <li>c) any of the Conditions Precedent have been breached or are not in full force and effect.</li>
    </ul>
    <h3>9. General provisions</h3>
    <ul>
      <li> a) Assignment – The Licensor may assign its rights and obligations under this Agreement by providing notice to the Licensee of the assignment. The Licensee may not assign its rights under this agreement without the Licensor’s consent.
      </li>
      <li> b) Entire agreement - This document contains the entire Agreement between the Parties in connection with its subject matter and supersedes all previous agreements and understandings except as otherwise provided herein.
      </li>
      <li> c) Further assurances – Further documents as may be necessary to ensure that this Agreement complies with the laws of any jurisdiction that is outside the jurisdiction as required.
      </li>
      <li> d) Governing law and jurisdiction – The laws of Queensland and Australia govern this Agreement. The Parties submit to the non-exclusive jurisdiction of the Supreme Court of Queensland and the Federal Court of Australia.</li>
      <li>e) Notices - a notice, demand, consent, approval or communication under this Agreement must be:
        <ul>
          <li>i. in writing, in English and signed by a person duly authorised by the sender; and</li>
          <li>ii. hand delivered, sent by prepaid post, facsimile or email, to the recipient&#39;s address for notices, as varied by any notice given by the recipient to the sender.</li>
        </ul>
        A Notice given in accordance with this clause takes effect when taken to be received (or at a later time specified in it), and is otherwise taken to be received:
        <ul>
          <li>i. if hand delivered, on delivery;</li>
          <li>ii. if sent by prepaid post, on the second Business Day after the date of posting (or on the seventh Business Day after the date of posting if posted to or from a place outside Australia);</li>
          <li>iii. if sent by facsimile, when the sender&#39;s facsimile system generates a message confirming successful transmission of the entire notice unless, within eight (8) business hours after the transmission, the recipient informs the sender that it has not received the entire notice, but if the delivery, receipt or transmission is not on a business day or is after 5.00pm on a business day, the notice is taken to be received at 9.00am on the next business day.</li>
        </ul>
      </li>
      <li>f) Severance - If anything in this Agreement is unenforceable, illegal or void then it is severed and the rest of this Agreement remains in full force and effect.</li>
      <li>g) Survival - Any clause which is expressed to survive, or which by its nature is intended to survive termination of this Agreement, survives termination.</li>
      <li>h) Variation - An amendment or variation to this Agreement is not effective unless it is in writing and agreed to by the Parties.</li>
      <li>i) Waiver - A Party’s failure or delay to exercise a power or right does not operate as a waiver of that power or right. A waiver is not effective unless it is in writing and signed by the Party giving it.</li>
    </ul>
  </div>
</div>
