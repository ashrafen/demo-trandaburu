<?php

/**
 * Implements template_preprocess_html().
 */
function omtheme_preprocess_html(&$variables) {
}

/**
 * Implements template_preprocess_page.
 */
function omtheme_preprocess_page(&$variables) {
}

/**
 * Implements template_preprocess_node.
 */
function omtheme_preprocess_node(&$variables) {
}
